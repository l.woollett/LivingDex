package me.lwoollett.livingdex.Model;

import android.graphics.Bitmap;

public class Pokemon {
    private int id;
    private String name;
    private boolean caught;
    private Bitmap image;
    public Pokemon(int id, String name){
        this.id = id;
        this.name = name.replaceFirst("([A-Z])(.*)", "$1\\u$2");
        this.caught = false;
        this.image = null;
    }

    public int getId(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
    public boolean isCaught() {
        return caught;
    }
    public Bitmap getImage() {return image;}

    public void setImage(Bitmap image){
        this.image = image;
    }

    public void setCaught(boolean caught) {
        this.caught = caught;
    }

    public String toString(){
        String toReturn = "";
        toReturn += this.id;
        toReturn += ": " + this.name;
        return toReturn;
    }
}
