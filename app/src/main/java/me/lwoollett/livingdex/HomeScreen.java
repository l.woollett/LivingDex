package me.lwoollett.livingdex;

import android.os.Bundle;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import me.lwoollett.livingdex.Adapters.PokemonAdapter;
import me.lwoollett.livingdex.Model.Pokemon;
import me.sargunvohra.lib.pokekotlin.client.PokeApi;
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;

public class HomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PokeApi api = new PokeApiClient();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        //Get the recycler view
        RecyclerView rvPokemon = (RecyclerView) findViewById(R.id.recyclerPokemon);
        ArrayList<Pokemon> pokes = new ArrayList<>();
        try {
            InputStream is = getResources().openRawResource(R.raw.data1); //In the raw folder
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String line = "";
            while ((line = reader.readLine()) != null) {
                    // Split the line into different tokens / items (using the comma as a separator).
                    String[] tokens = line.split(",");
                    pokes.add(new Pokemon(Integer.parseInt(tokens[0]), tokens[1]));
                    //Add the pokemon into the list
                }

        }catch (FileNotFoundException e){
            Log.e("IO", "Data file not found.");
        }catch (IOException e){
            Log.e("IO", e.getMessage());
        }
        //pokes.get(2).setImage();
        PokemonAdapter adapter = new PokemonAdapter(pokes); //Create the adapter
        rvPokemon.setAdapter(adapter); //Set the adapter to the recycler view

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
        rvPokemon.setLayoutManager(gridLayoutManager); //Dunno why i have to do this
        //But i do.
    }
}
