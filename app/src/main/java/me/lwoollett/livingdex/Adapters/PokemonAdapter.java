package me.lwoollett.livingdex.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import me.lwoollett.livingdex.Model.Pokemon;
import me.lwoollett.livingdex.R;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> {
    private List<Pokemon> storedPokemon; //Our list of pokemon.
    private Context context; //Used for toast calls

    private boolean multiselected = false;

    public PokemonAdapter(List<Pokemon> pokes) { //When created, we give it a list of pokemon
        storedPokemon = pokes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_pokemon, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(context, contactView);
        return viewHolder;
    }

    //Populate the view with what's in the list at the given position.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Get the data based on position
        final Pokemon selected = storedPokemon.get(position);
        TextView idV = holder.idTextView;
        TextView nameV = holder.nameTextView;
        ImageView spriteV = holder.spriteView;
        idV.setText(String.valueOf(selected.getId()));
        nameV.setText(selected.getName());
        //Load the image directly into the imageview
        Picasso.get().load("https://raw.githubusercontent.com/lwoollett/LivingDex/master/Img/" + selected.getId() + ".png").into(spriteV);
    }

    @Override
    public int getItemCount() {
        return storedPokemon.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameTextView;
        public TextView idTextView;
        public ImageView spriteView;
        private Context context;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(Context context, View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            idTextView = itemView.findViewById(R.id.pokemon_id);
            nameTextView = itemView.findViewById(R.id.pokemon_name);
            spriteView = itemView.findViewById(R.id.sprite_view);
            this.context = context;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                Pokemon pokemon = storedPokemon.get(position);
                Toast.makeText(context, pokemon.getName(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public boolean onLongClick(View v){
            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION){
                Pokemon pokemon = storedPokemon.get(position);
                Toast.makeText(context, pokemon.getName() + " was long pressed", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    }
}
