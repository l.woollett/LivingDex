package me.lwoollett.livingdex.Adapters;

import android.view.View;

interface ViewHolder {
    void onClick(View v);

    void onLongClick(View v);
}
